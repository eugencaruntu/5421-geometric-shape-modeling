/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include "Isosceles.h"
#include <math.h>

using namespace std;

Isosceles::Isosceles(int b, const std::string& shapeDescrName) : Triangle("Isosceles Triangle", shapeDescrName)
{
	if (b >= 1)
	{
		if (b % 2 == 0)
		{
			setB(b + 1);
		}
		else
		{
			setB(b);
		}
		setH((getB() + 1) / 2);
	}
	else throw string("Base ('" + to_string(b) + "') must be a positive integer greater or equal to 1");
}

void Isosceles::scale(const int & factor)
{
	if ((getB() + 2 * factor) >= 1)
	{
		setB(getB() + 2 * factor);
		setH((getB() + 1) / 2);
	}
	else throw string("Scale factor ('" + to_string(factor) + "') can't be applied to current base ('" + to_string(getB()) + "').");
}

int Isosceles::getScrArea() const
{
	return getH()*getH();
}

int Isosceles::getScrPerimeter() const
{
	return 4 * (getH() - 1);
}

double Isosceles::getGeoPerimeter() const
{
	return getB() + 2 * sqrt(0.25*getB()*getB() + getH()*getH());
}

vector<vector<char>> Isosceles::draw(char penChar, char fillChar) const
{
	size_t c;		// character in a row
	size_t z;		// z is the amount of penChars per line
	size_t mid;		// middle of row
	vector<vector<char>> grid{ Shape::draw() };			// prepare the grid
	for (size_t r = 0; r < grid.size(); ++r)			// fill the vector
	{
		mid = grid[r].size() / 2 + 1;
		z = r * 2 + 1;
		for (c = 0; c < mid - z / 2 - 1; ++c) { grid[r][c] = fillChar; }			// fill
		for (c = mid - z / 2 - 1; c < mid + z / 2; ++c) { grid[r][c] = penChar; }	// pen
		for (c = mid + z / 2; c < grid[r].size(); ++c) { grid[r][c] = fillChar; }	// fill
	}
	return grid;
}
