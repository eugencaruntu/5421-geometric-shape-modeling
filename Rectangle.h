/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "Shape.h"

class Rectangle : public Shape
{
private:
	int h;
	int w;

public:
	Rectangle(int h, int w, const std::string& shapeDescrName = "Generic Rectangle");
	~Rectangle() = default;
	virtual void scale(const int& factor) override;
	virtual int getBoudingBoxHeight() const override;
	virtual int getBoundingBoxWidth() const override;
	virtual double getGeoArea() const override;
	virtual int getScrArea() const override;
	virtual double getGeoPerimeter() const override;
	virtual int getScrPerimeter() const override;
	virtual std::vector<std::vector<char>> draw(char penChar = '*', char fillChar = ' ') const override;	// makes a vector grid from invoking object
};
#endif