/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#ifndef RHOMBUS_H
#define RHOMBUS_H

#include "Shape.h"

class Rhombus : public Shape
{
private:
	int d;

public:
	Rhombus(int d, const std::string& shapeDescrName = "Generic Rhombus");
	~Rhombus() = default;
	virtual void scale(const int& factor) override;
	virtual int getBoudingBoxHeight() const override;
	virtual int getBoundingBoxWidth() const override;
	virtual double getGeoArea() const override;
	virtual int getScrArea() const override;
	virtual double getGeoPerimeter() const override;
	virtual int getScrPerimeter() const override;
	virtual std::vector<std::vector<char>> draw(char penChar = '*', char fillChar = ' ') const override;
};

#endif