# Geometric Shape Modeling

Using simple geometric shapes, this assignment will give you practice with fundamental
principles of OOP: encapsulation, inheritance and polymorphism. The geometric shapes
considered are simple two-dimensional shapes that can be reasonably depicted textually
on the computer screen, such as squares, rectangles, and specific kinds of triangles and
rhombuses.