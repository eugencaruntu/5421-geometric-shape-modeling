/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include "SlotMachine.h"
#include "Rectangle.h"
#include "Rhombus.h"
#include "RightTriangle.h"
#include "Isosceles.h"
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <time.h>

using namespace std;
using std::cout;

void SlotMachine::make_shapes()
{
	for (unsigned i = 0; i < shape_reel.size(); ++i)
	{
		make_shape(i);
	}
}

void SlotMachine::make_shape(size_t r)
{
	size_t n = rand() % 4;			// generate a random number between 0 and 3
	size_t w = rand() % 25 + 1;		// generate a random number between 1 and 25

	switch (n)
	{
	case 0: shape_reel[r] = new Rhombus(w); break;
	case 1: shape_reel[r] = new Isosceles(w); break;
	case 2: shape_reel[r] = new RightTriangle(w); break;
	case 3:
		size_t h = rand() % 25 + 1;	// generate a random number between 1 and 25
		shape_reel[r] = new Rectangle(w, h);
		break;
	}
}

void SlotMachine::display_shapes()
{
	size_t w{ 0 };
	size_t h{ 0 };

	for (Shape* v : shape_reel)	// cumulate the bouding boxes
	{
		w += v->getBoundingBoxWidth();	// adds each of the 3 shapes widths
		h = (h >= (size_t)v->getBoudingBoxHeight()) ? h : v->getBoudingBoxHeight();	// maximum from h and bouding box height
	}
	// resize to fit separators/dividers
	w += 10;
	h += 2;
	// add the 3 vectors into a vector grid
	vector<vector<char>> grid(h, vector<char>());
	for (size_t k = 0; k < grid.size(); ++k) { grid[k].resize(w); }	// resize to appropriate width
	vector<vector<char>> v0 = shape_reel[0]->draw('*', ' ');
	vector<vector<char>> v1 = shape_reel[1]->draw('*', ' ');
	vector<vector<char>> v2 = shape_reel[2]->draw('*', ' ');
	// determine the width of each 3 vectors on their first row
	size_t v0w = v0[0].size();
	size_t v1w = v1[0].size();
	size_t v2w = v2[0].size();

	size_t c{ 0 }; size_t g;	// to iterate columns for shapes and consolidated grid
	size_t r; size_t l;			// to iterate rows for shapes and consolidated grid

	// add first and last rows
	for (g = 0; g < w; ++g)
	{
		if (g == 0 || g == v0w + 3 || g == v0w + v1w + 6 || g == w - 1)
		{
			grid[0][g] = '+';		// first
			grid[h - 1][g] = '+';	// last
		}
		else
		{
			grid[0][g] = '-';		// first
			grid[h - 1][g] = '-';	// last
		}
	}
	// add shapes and separators row by row, column by column
	for (r = 0; r < (size_t)(h - 2); ++r)
	{
		l = r + 1;	// advance row since we already populated the top row
		g = 0;
		// add begin border
		grid[l][g] = '|';
		grid[l][++g] = ' ';
		// continue with first shape
		for (c = 0; c < v0w; ++c)
		{
			if (r < v0.size()) { grid[l][++g] = v0[r][c]; }
			else grid[l][++g] = ' ';
		}
		// add divider
		grid[l][++g] = ' ';
		grid[l][++g] = '|';
		grid[l][++g] = ' ';
		// continue with second shape
		for (c = 0; c < v1w; ++c)
		{
			if (r < v1.size()) { grid[l][++g] = v1[r][c]; }
			else grid[l][++g] = ' ';
		}
		// add divider
		grid[l][++g] = ' ';
		grid[l][++g] = '|';
		grid[l][++g] = ' ';
		// continue with last shape
		for (c = 0; c < v2w; ++c)
		{
			if (r < v2.size()) { grid[l][++g] = v2[r][c]; }
			else grid[l][++g] = ' ';
		}
		//add end border
		grid[l][++g] = ' ';
		grid[l][++g] = '|';
	}
	cout << grid;
	cout << endl;
}

void SlotMachine::report_status()
{
	// set the outcome
	outcome = loss;
	if (typeid(*shape_reel[0]).name() == typeid(*shape_reel[1]).name() || typeid(*shape_reel[0]).name() == typeid(*shape_reel[2]).name() || typeid(*shape_reel[1]).name() == typeid(*shape_reel[2]).name())
	{
		outcome = neutral;
	}

	if (typeid(*shape_reel[0]).name() == typeid(*shape_reel[1]).name() && typeid(*shape_reel[0]).name() == typeid(*shape_reel[2]).name())
	{
		outcome = win2;

		if (shape_reel[0]->getGeoArea() == shape_reel[1]->getGeoArea() && shape_reel[0]->getGeoArea() == shape_reel[2]->getGeoArea())
		{
			outcome = win3;
		}
	}

	switch (outcome) {
	case loss: message = "You lose your bet: " + to_string(bet); tokens -= bet; break;
	case neutral: message = "You don't win, you don't lose, your are safe!"; break;
	case win2: message = "Congratulations! You win 2 times your bet: " + to_string(bet * 2); tokens += (bet * 2); break;
	case win3: message = "Congratulations! You win 3 times your bet: " + to_string(bet * 3); tokens += (bet * 3); break;
	}

	for (Shape* s : shape_reel)
	{
		cout << "(" << s->getShapeName() << ", " << s->getBoundingBoxWidth() << ", " << s->getBoudingBoxHeight() << ")  ";
	}
	cout << endl << message << endl;
	cout << "You now have " << tokens << " tokens!" << endl;
	cout << endl;
}

void SlotMachine::release_shapes()
{
	for (size_t i = 0; i < shape_reel.size(); i++)
	{
		if (shape_reel[i] != nullptr) // since this is used in destructor, we check just in case we would go out of scope if no Shapes are assigned to array indices
		{
			delete shape_reel[i];
			shape_reel[i] = nullptr;
		}
	}
}

void SlotMachine::run()
{
	srand((unsigned)time(NULL));	// seed the random number generator

	/* User dialog */
	tokens = 10;
	cout << "Welcome to this 3 - Reel Slot Machine Game!\n"s +
		"Each reel will randomly display one of four shapes, each in 25 sizes.\n" +
		"To win 3 times your bet you need 3 similar shapes of the same size .\n" +
		"To win 2 times your bet you need 3 similar shapes.\n" +
		"To win or lose nothing you need 2 similar shapes.\n" +
		"Otherwise, you lose your bet.\n"
		"You start with " << tokens << " free tokens!" << endl << endl;

	do {
		bool valid = false;
		do {
			cout << "How much would you like to bet (enter 0 to quit)? ";
			getline(cin, input);
			if (!cin.good()) {
				valid = false;
				cin.clear();
				cin.ignore(numeric_limits<streamsize>::max(), '\n');
			}
			// do not allow alphanumeric inputs (read entire line)
			size_t foundAlpha = input.find_first_not_of("0123456789");
			if (input.size() == 0 || foundAlpha != string::npos)
			{
				valid = false;
				cout << "\tYou must enter a positive number." << endl;
			}
			// convert the input to bets
			else
			{
				bet = stoi(input);
				if (bet > tokens)	// do not allow user to bet more than the amount of tokens 
				{
					valid = false;
					cout << "\tYour bet is limited by the amount of tokens you own. You have " << tokens << " tokens." << endl;
				}
				else
				{
					valid = true;
				}
			}
		} while (!valid);

		if (bet != 0 && bet <= tokens)
		{
			make_shapes();
			display_shapes();
			report_status();
			release_shapes();
		}

	} while (bet != 0 && tokens > 0);

	// quit the game if user wants to quit or if user has no more tokens
	cout << "Game over. You now have " << tokens << " tokens!" << endl;
}

SlotMachine::~SlotMachine()
{
	this->release_shapes();
}
