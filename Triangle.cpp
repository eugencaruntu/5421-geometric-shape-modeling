/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include "Triangle.h"
#include <math.h>

using namespace std;

Triangle::Triangle(const std::string & shapeName, const std::string & shapeDescrName) : Shape(shapeName, shapeDescrName)
{
}

void Triangle::setB(int b)
{
	this->b = b;
}

void Triangle::setH(int h)
{
	this->h = h;
}

int Triangle::getB() const
{
	return this->b;
}

int Triangle::getH() const
{
	return this->h;
}

int Triangle::getBoundingBoxWidth() const
{
	return this->getB();
}

int Triangle::getBoudingBoxHeight() const
{
	return this->getH();
}

double Triangle::getGeoArea() const
{
	return ((double)(getH()*getB()) / 2);
}
