/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include "RightTriangle.h"
#include <math.h>

using namespace std;

RightTriangle::RightTriangle(int b, const std::string & shapeDescrName) : Triangle("Right Triangle", shapeDescrName)
{
	if (b >= 1)
	{
		this->setB(b);
		this->setH(b);
	}
	else throw string("Base ('" + to_string(b) + "') must be a positive integer greater or equal to 1");
}

void RightTriangle::scale(const int & factor)
{
	if ((getB() + factor) >= 1)
	{
		this->setB(getB() + factor);
		this->setH(getB() + factor);
	}
	else throw string("Scale factor ('" + to_string(factor) + "') can't be applied to current base ('" + to_string(getB()) + "').");
}

int RightTriangle::getScrArea() const
{
	return getH()*(getH() + 1) / 2;
}

int RightTriangle::getScrPerimeter() const
{
	return 3 * (getH() - 1);
}

double RightTriangle::getGeoPerimeter() const
{
	return (2 + sqrt(2))*getH();
}

vector<vector<char>> RightTriangle::draw(char penChar, char fillChar) const
{
	size_t c;		// character in a row
	size_t z;		// z is the amount of penChars per line
	vector<vector<char>> grid{ Shape::draw() };			// prepare the grid
	for (size_t r = 0; r < grid.size(); ++r)			// fill the vector
	{
		z = r + 1;
		for (c = 0; c < z; ++c) { grid[r][c] = penChar; }				// pen
		for (c = z; c < grid[r].size(); ++c) { grid[r][c] = fillChar; }	// fill
	}
	return grid;
}
