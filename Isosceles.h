/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#ifndef ISOSCELES_H
#define ISOSCELES_H

#include "Triangle.h"

class Isosceles : public Triangle
{
private:

public:
	Isosceles(int b, const std::string& shapeDescrName = "Generic Isosceles Triangle");
	virtual ~Isosceles() = default;
	virtual void scale(const int& factor) override;
	virtual int getScrArea() const override;
	virtual double getGeoPerimeter() const override;
	virtual int getScrPerimeter() const override;
	virtual std::vector<std::vector<char>> draw(char penChar = '*', char fillChar = ' ') const override;
};

#endif