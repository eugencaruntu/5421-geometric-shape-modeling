COMP 5421 :: Assignment 4
Eugen Caruntu #29077103

DRIVER:
shape_driver.cpp is set with the SlotMachine driver. There is a commented part to support the Shape driver as illustrated in instructions (pertinent includes are also commented out)

COMPILER:
Delivery was compiling correctly in VS 2015 and 2017. 
	VS 2013 note: the driver from instructions has a line that is not supported in VS2013 compiler, more exactly std::array<Shape*, 3> shape_reel{};

INHERITANCE DECISIONS:
	- Common contet was 'pushed-up' to base clases as much as possible. In this implementation there is a common part that each derived draw() implementation is calling from base class (Shape::draw() which is the common piece responsible for making en empty vector grid before painting the respective shape)
	- Derived clases out of Triangles will get and set their base and hight from Triangle
	- Ceiling the implementation of some base class functions was done using 'final' keyword, in order to prevent both masking and overriding in derived clases

<EOF>