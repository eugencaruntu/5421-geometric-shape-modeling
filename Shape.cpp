/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include "Shape.h"
#include <typeinfo>
#include <sstream>
#include <iostream>
#include <iomanip>

using namespace std;

size_t Shape::shapeID = 0;	// instantiate the shape ID counter

Shape::Shape(const std::string& shapeName, const std::string& shapeDescrName)
{
	setShapeName(shapeName);
	setShapeDescrName(shapeDescrName);
	setShapeID();
}

int Shape::getShapeID() const
{
	return Shape::shapeID;
}

void Shape::setShapeID() const
{
	++shapeID;
}

string Shape::getShapeName() const
{
	return shapeName;
}

string Shape::getShapeDescrName() const
{
	return shapeDescrName;
}

void Shape::setShapeName(const string& shapeName)
{
	this->shapeName = shapeName;
}

void Shape::setShapeDescrName(const string& shapeDescrName)
{
	this->shapeDescrName = shapeDescrName;
}

string Shape::toString() const
{
	setprecision(2);
	return
		"Shape Information\n"s +
		"------------------\n"
		"Static type:\t" + typeid(this).name() + "\n" +
		"Dynamic type:\t" + typeid(*this).name() + "\n" +
		"Generic name:\t" + this->getShapeName() + "\n" +
		"Description:\t" + this->getShapeDescrName() + "\n" +
		"id:\t\t" + to_string(this->getShapeID()) + "\n" +
		"B.box width:\t" + to_string(this->getBoundingBoxWidth()) + "\n" +
		"B.box height:\t" + to_string(this->getBoudingBoxHeight()) + "\n" +
		"Scr area:\t" + to_string(this->getScrArea()) + "\n" +
		"Geo area:\t" + to_string_with_precision(this->getGeoArea(), 2) + "\n" +
		"Scr perimeter:\t" + to_string(this->getScrPerimeter()) + "\n" +
		"Geo perimeter:\t" + to_string_with_precision(this->getGeoPerimeter(), 2) + "\n";
}

ostream& operator<<(ostream& sout, const Shape& shape)
{
	sout << shape.toString() << endl;
	return sout;
}

ostream& operator<<(ostream& sout, const vector<vector<char>>& grid)
{
	for (vector<char> v : grid)
	{
		for (char ch : v)
		{
			sout << ch;
		}
		sout << '\n';
	}
	return sout;
}

string Shape::to_string_with_precision(const double& nbr, const int n) const
{
	ostringstream out;
	out << setprecision(n) << fixed << nbr;
	return out.str();
}

void Shape::draw_on_screen(char penChar, char fillChar) const
{
	cout << this->draw(penChar, fillChar) << endl;
}

vector<vector<char>> Shape::draw(char penChar, char fillChar) const
{
	int grid_h = this->getBoudingBoxHeight();
	int grid_w = this->getBoundingBoxWidth();
	vector<vector<char>> grid;
	grid.resize(grid_h);						// set rows
	for (size_t k = 0; k < grid.size(); ++k)	// set columns
	{
		grid[k].resize(grid_w);
	}
	return grid;
}