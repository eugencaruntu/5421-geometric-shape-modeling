/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Shape.h"

class Triangle : public Shape
{
private:
	int b;
	int h;

public:
	Triangle(const std::string & shapeName = "Triangle", const std::string & shapeDescrName = "Generic Triangle");
	~Triangle() = default;
	virtual void setB(int b) final;
	virtual void setH(int h) final;
	virtual int getB() const final;
	virtual int getH() const final;
	virtual int getBoundingBoxWidth() const override final;
	virtual int getBoudingBoxHeight() const override final;
	virtual double getGeoArea() const override final;
	virtual std::vector<std::vector<char>> draw(char penChar = '*', char fillChar = ' ') const = 0;
	virtual void scale(const int& factor) = 0;
	virtual int getScrArea() const = 0;
	virtual double getGeoPerimeter() const = 0;
	virtual int getScrPerimeter() const = 0;
};

#endif