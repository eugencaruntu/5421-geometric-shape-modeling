/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include "Rectangle.h"
#include <type_traits>

using namespace std;

Rectangle::Rectangle(int w, int h, const string& shapeDescrName) : Shape("Rectangle", shapeDescrName)
{
	if (w >= 1 && h >= 1)
	{
		this->w = w;
		this->h = h;
	}
	else throw string("Height ('" + to_string(h) + "') and width ('" + to_string(w) + "') must be positive integers greater or equal to 1");
}

void Rectangle::scale(const int& factor)
{
	if ((w + factor) >= 1 && (h + factor) >= 1)
	{
		this->w = w + factor;
		this->h = h + factor;
	}
	else throw string("Scale factor ('" + to_string(factor) + "') can't be applied to current width ('" + to_string(w) + "') and height ('" + to_string(h) + "').");
}

int Rectangle::getBoudingBoxHeight() const
{
	return h;
}

int Rectangle::getBoundingBoxWidth() const
{
	return w;
}

double Rectangle::getGeoArea() const
{
	return h*w;
}

int Rectangle::getScrArea() const
{
	return h*w;
}

double Rectangle::getGeoPerimeter() const
{
	return 2 * (h + w);
}

int Rectangle::getScrPerimeter() const
{
	return 2 * (h + w) - 4;
}

vector<vector<char>> Rectangle::draw(char penChar, char fillChar) const
{
	vector<vector<char>> grid{ Shape::draw() };			// prepare the grid

	for (size_t r = 0; r < grid.size(); ++r)			// fill the vector
	{
		for (size_t c = 0; c < grid[r].size(); ++c)
		{
			grid[r][c] = penChar;
		}
	}
	return grid;
}
