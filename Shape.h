/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#ifndef SHAPE_H
#define SHAPE_H
#include <string>
#include <vector>

class Shape
{
private:
	static std::size_t shapeID;		// a distinct identity number, an integer (constant value incremented in constructors)
	std::string shapeName;			// a generic name such as "Rectangle"
	std::string shapeDescrName;		// a descriptive name, such as "Swimming Pool"
	/* private helper functions */
	std::string to_string_with_precision(const double& nbr, const int n) const;

public:
	/* Constructors
	each derived class would add their respective validatin and delegate to base ctor */
	Shape(const std::string& shapeName, const std::string& shapeDescrName);

	/* Disabling default and copy ctor and assignment operator
	Compiler wont provide ctors since we already defined at least one, but nevertheless ... */
	Shape() = delete;
	Shape& operator=(const Shape&) = delete;
	Shape(const Shape&) = delete;

	/* Destructor */
	virtual ~Shape() = default;

	/* these member functions need to be ceiled in base
	(ensure they are not being overriden nor masked in derived classes by using final)
	Modifier member functions would be used in constructors as needed.
	Some validation might be done while implemented in base class */
	virtual int	getShapeID() const final;						// return the shape ID
	virtual void setShapeID() const final;						// sets the shape ID and must only be used in constructor
	virtual std::string getShapeName() const final;				// return the shape descriptive name
	virtual std::string getShapeDescrName() const final;		// return the shape descriptive name
	virtual void setShapeName(const std::string&) final;		// set the shape name
	virtual void setShapeDescrName(const std::string&) final;	// set the shape descriptive name

	virtual std::string toString() const final;					// Returns a string from an invoking shape. Will be implemented ONLY in base class (any different attributes will be obtain through polymorphism)
	virtual void draw_on_screen(char penChar = '*', char fillChar = ' ') const final;	// Outputs on screen the stream obtained from vector.draw() (it is a wrapper function)
	friend std::ostream& operator<<(std::ostream& sout, const Shape& shape);			// Outputs on screen the to_string() from invoking shape (represents Opearator<< overload for shape)
	friend std::ostream& operator<<(std::ostream& sout, const std::vector<std::vector<char>>& grid); // Outputs on screen the vector.draw(grid) (represents Opearator<< overload for shape)

	/* Pure virtual functions
	 All of them must be implemented in next derived class
	 Once implemented in derived class they would be ceiled (making them final and avoid their implementation lower in hierarchy)
	 Some would have an implementation in base class and will be used in derived to add specifics */
	virtual void scale(const int& factor) = 0;		// scale the shape by a given integer factor
	virtual int getBoudingBoxHeight() const = 0;	// the height of the shape's bounding box
	virtual int getBoundingBoxWidth() const = 0;	// the width of the shape's bounding box
	virtual double getGeoArea() const = 0;			// the geometric area of the shape
	virtual int getScrArea() const = 0;				// the screen area of the shape (the number of characters that form the textual image of the shape)
	virtual double getGeoPerimeter() const = 0;		// the geometric perimeter of the shape
	virtual int getScrPerimeter() const = 0;		// the screen perimeter of the shape (the number of characters on the borders of the textual image of the shape)
	
	// draw a textual image of the shape on a given two dimensional grid
	virtual std::vector<std::vector<char>> draw(char penChar = '*', char fillChar = ' ') const = 0;	// base implementation makes a vector grid from invoking object (will only take care of the common part of coreting an empty vector of vectors)
};

#endif