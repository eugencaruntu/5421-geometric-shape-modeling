/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#ifndef SLOTMACHINE_H
#define SLOTMACHINE_H
#include "Shape.h"
#include <array>

class SlotMachine
{
private:
	std::array<Shape*, 3> shape_reel{};		// an array of 3 pointers to Shape
	unsigned tokens;
	unsigned bet;
	std::string input;
	std::string message;
	enum result{loss, neutral, win2, win3};
	SlotMachine::result outcome;

	// implemets step 2
	void make_shapes();						// makes shape reels point at newly created dynamic shape objects

	// implemets step 2.1
	void make_shape(size_t r);				// makes shape_reel[r] point at a newly created dynamic shape object

	// implements step 3
	void display_shapes();					// displays the shape reels

	// implements step 4
	void report_status();					// displays outcome, payout, and tokens left

	// implements step 5
	void release_shapes();					// frees dynamic objects currently pointed at by the shape reels

public:
	// enable default constructor
	SlotMachine() = default;

	// disable copy constructor and assignment
	SlotMachine(const SlotMachine&) = delete;
	SlotMachine& operator=(const SlotMachine&) = delete;

	void run();								// implements the algorithm described above
	virtual ~SlotMachine();					// frees dynamic objects currently pointed at by the shape reels
};
#endif