/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include "Rhombus.h"
#include <math.h>

using namespace std;

Rhombus::Rhombus(int d, const std::string & shapeDescrName) : Shape("Rhombus", shapeDescrName)
{
	if (d >= 1)
	{
		if (d % 2 == 0) { this->d = d + 1; }
		else { this->d = d; }
	}
	else throw string("Diagonal ('" + to_string(d) + "') must be a positive integer greater or equal to 1");
}

void Rhombus::scale(const int & factor)
{
	if ((d + 2 * factor) >= 1)
	{
		this->d = d + 2 * factor;
	}
	else throw string("Scale factor ('" + to_string(factor) + "') can't be applied to current diagonal ('" + to_string(d) + "').");
}

int Rhombus::getBoundingBoxWidth() const
{
	return d;
}

int Rhombus::getBoudingBoxHeight() const
{
	return d;
}

int Rhombus::getScrArea() const
{
	double n = floor(d / 2);
	return static_cast<int>(2 * n*(n + 1) + 1);
}

int Rhombus::getScrPerimeter() const
{
	return (2 * (d - 1));
}

double Rhombus::getGeoArea() const
{
	return ((d*d) / 2);
}

double Rhombus::getGeoPerimeter() const
{
	return (2 * sqrt(2)*d);
}

vector<vector<char>> Rhombus::draw(char penChar, char fillChar) const
{
	size_t c;		// character in a row
	size_t z;		// z is the amount of penChars per line
	size_t mid;		// middle of row
	vector<vector<char>> grid{ Shape::draw() };			// prepare the grid
	for (size_t r = 0; r < grid.size(); ++r)			// fill the vector
	{
		mid = grid[r].size() / 2 + 1;					// middle of vector
		if (r <= grid.size() / 2) { z = r * 2 + 1; }	// top part
		else { z = (grid[r].size() - r - 1) * 2 + 1; }	// bottom part
		for (c = 0; c < mid - z / 2 - 1; ++c) { grid[r][c] = fillChar; }			// fill
		for (c = mid - z / 2 - 1; c < mid + z / 2; ++c) { grid[r][c] = penChar; }	// pen
		for (c = mid + z / 2; c < grid[r].size(); ++c) { grid[r][c] = fillChar; }	// fill
	}
	return grid;
}
