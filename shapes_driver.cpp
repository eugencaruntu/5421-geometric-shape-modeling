/*
Created by eugen on 7/11/2017
COMP 5421 :: Assignment 4
Eugen Caruntu #29077103
*/

#include <iostream>
#include <exception>

/* includes for shapesDriver */
#include "Rectangle.h"
#include "Rhombus.h"
#include "Isosceles.h"
#include "RightTriangle.h"

/* includes for slotMachineDriver */
#include "SlotMachine.h"

using namespace std;

void shapesDriver()
{
	try
	{
		Rectangle shape1(10, 3);
		cout << shape1 << endl;
		Rectangle shape2(10, 15);
		cout << shape2 << endl;
		/* Rhombus */
		Rhombus ace(16, "Ace of diamond");
		cout << ace.toString() << endl;
		/* or, equivalently: */
		cout << ace << endl;
		/* Isosceles */
		Isosceles iso(17);
		/* the following call is polymorphic but iso is neither a reference nor a pointer */
		cout << iso << endl; // it uses the operator<< overload from base class Shape
							 /* equivalently: */
		Shape *isoptr = &iso;
		cout << *isoptr << endl; // polymorphic call
		Shape &isoref = iso;
		cout << isoref << endl; // polymorphic call
								/* Right Triangle */
		RightTriangle rt(10, "Carpenter's square");
		cout << rt << endl;
		cout << shape1.draw() << endl;
		cout << ace.draw('o') << endl;
		cout << iso.draw('\\', '.') << endl;
		cout << rt.draw('+', '-') << endl;
		ace.draw_on_screen(' ', 'o');
		ace.scale(-4);
		ace.draw_on_screen('1');
		ace.scale(2);
		ace.draw_on_screen('A', '.');
	}
	catch (string msg) { cout << msg << endl; }
	catch (exception& e) { cout << "Standard exception: " << e.what() << endl; exit(EXIT_FAILURE); }
	system("pause");
}

int main()
{
	/* SHAPES DRIVER */
	//shapesDriver();
	/* END SHAPES DRIVER*/

	/* SLOT MACHINE DRIVER */
	SlotMachine slot_machine;	// create a slot machine object
	try
	{
		slot_machine.run();		// run the slot machine until the player decides to stop, or until the player runs out of tokens
	}
	catch (string msg) { cout << msg << endl; }
	catch (exception& e) { cout << "Standard exception: " << e.what() << endl;}
	system("pause");
	/* END SLOT MACHINE DRIVER */

	return EXIT_SUCCESS;
}

